package com.mobility.cariq.device.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Random;

import com.mobility.cariq.device.interfaces.ConnectionStatus;
import com.mobility.cariq.device.interfaces.Device;
import com.mobility.cariq.device.interfaces.FileType;
import com.mobility.cariq.device.interfaces.DeviceAdapterException;


/**
 * The Class DefaultDevice.
 */
public class DefaultDevice implements Device{

	/** The device id. */
	String deviceId;
	
	/** The device url. */
	String deviceUrl;
	
	/** The stream. */
	String stream;
	
	/** The response. */
	String response;
	
	/** The command map. */
	HashMap <String , String> commandMap;
	
	/** The command response map. */
	HashMap <HashMap<String, String>,Object> commandResponseMap;
	
	/** The file. */
	File file;
	
	/** The file type. */
	FileType fileType;
	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceUrl() {
		return deviceUrl;
	}

	public void setDeviceUrl(String deviceUrl) {
		this.deviceUrl = deviceUrl;
	}

	public HashMap<String, String> getCommandMap() {
		return commandMap;
	}

	public void setCommandMap(HashMap<String, String> commandMap) {
		this.commandMap = commandMap;
	}

	public HashMap<HashMap<String, String>, Object> getCommandResponseMap() {
		return commandResponseMap;
	}

	public void setCommandResponseMap(
			HashMap<HashMap<String, String>, Object> commandResponseMap) {
		this.commandResponseMap = commandResponseMap;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public ConnectionStatus getConnectionStatus() {
		return connectionStatus;
	}

	public void setConnectionStatus(ConnectionStatus connectionStatus) {
		this.connectionStatus = connectionStatus;
	}

	/** The connection status. */
	ConnectionStatus connectionStatus;

	
	public DefaultDevice(String deviceUrl) {
		this.deviceUrl =  deviceUrl;
		this.deviceId = Integer.toHexString(new Random(28373).nextInt());
	}

	public DefaultDevice(String deviceId, String deviceUrl, String stream,
			String response, HashMap<String, String> commandMap,
			HashMap<HashMap<String, String>, Object> commandResponseMap,
			File file, FileType fileType, ConnectionStatus connectionStatus) {
		super();
		this.deviceId = deviceId;
		this.deviceUrl = deviceUrl;
		this.stream = stream;
		this.response = response;
		this.commandMap = commandMap;
		this.commandResponseMap = commandResponseMap;
		this.file = file;
		this.fileType = fileType;
		this.connectionStatus = connectionStatus;
	}

	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.Device#getId()
	 */
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return this.deviceId;
	}
	
	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.Device#connect()
	 */
	@Override
	public void connect() throws DeviceAdapterException {
		try{
			if(this.connectionStatus.equals(ConnectionStatus.CONNECTED)){
				throw new DeviceAdapterException("Exception: Device is already Connected!");
			}
			else if(this.connectionStatus.equals(ConnectionStatus.UNKNOWN)){
				throw new DeviceAdapterException("Exception: Device State Unknown!");
			}
			else if(this.connectionStatus.equals(ConnectionStatus.DISCONNECTED)){
				this.connectionStatus = ConnectionStatus.CONNECTED;
			}
		}catch(DeviceAdapterException e){
			throw new DeviceAdapterException("Exception occurred at com.mobility.cariq.device.impl.DefaultDevice: connect()", e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.Device#getStatus()
	 */
	@Override
	public ConnectionStatus getStatus() throws DeviceAdapterException {
		// TODO Auto-generated method stub
		return this.connectionStatus;
	}
	
	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.Device#disconnect()
	 */
	@Override
	public void disconnect() throws DeviceAdapterException {
		try{
			if(this.connectionStatus.equals(ConnectionStatus.DISCONNECTED)){
				throw new DeviceAdapterException("Exception: Device Alredy Disconnected, attempt to connect first!");
			}
			else if (this.connectionStatus.equals(ConnectionStatus.UNKNOWN)){
				throw new DeviceAdapterException("Exception: Device Status unknown!");
			}
			else if(this.connectionStatus.equals(ConnectionStatus.CONNECTED)){
				//should handle garbage collection, object.destry()
				this.connectionStatus = ConnectionStatus.DISCONNECTED;		
			}
		}catch(DeviceAdapterException e){
			throw new DeviceAdapterException("Exception occurred at com.mobility.cariq.device.impl.DefaultDevice: disconnect()!", e); 
		}		
	}
	
	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.Device#readFile(java.lang.String, com.mobility.cariq.device.interfaces.FileType)
	 */
	@Override
	public File readFile(String fileId, FileType fileType) throws DeviceAdapterException {

		try{
			if(!(this.getStatus().equals(ConnectionStatus.CONNECTED) )){
				throw new DeviceAdapterException("EXCEPTION: Device is Not Connected!");
			}
			else{
				if(!(this.file.exists())){
					throw new FileNotFoundException("EXCEPTION: File Does Not Exist!");
				}
				else{
					if(!(this.fileType == fileType)){
						throw new FileNotFoundException("Exception: Wrong File Type!");
					}
					else{
						return this.file;
					}
				}
			}	
		}catch(Exception e){
			throw new DeviceAdapterException("Exception occurred at com.mobility.cariq.device.impl.DefaultDevice: readFIle()!", e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.Device#writeFile(java.io.File, com.mobility.cariq.device.interfaces.FileType)
	 */
	@Override
	public String writeFile(File file, FileType fileType) throws DeviceAdapterException {
		try{
			if(!(this.getStatus().equals(ConnectionStatus.CONNECTED))){
				throw new FileNotFoundException("Exception: Device is Not COnnected!");
			}
			else{
				this.file = file;
				this.fileType = fileType;
				return this.file.getName();
			}
		}catch(Exception e){
			throw new DeviceAdapterException("Exception occurred at com.mobility.cariq.device.impl.DefaultDevice: writeFile()!", e);
		}		
	}
	
	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.Device#read()
	 */
	@Override
	public String read() throws DeviceAdapterException {
		// TODO Auto-generated method stub
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.Device#read(java.lang.String)
	 */
	@Override
	public String read(String stream) throws DeviceAdapterException {
		// TODO Auto-generated method stub
		return this.stream;
	}
	
	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.Device#write(java.lang.String, java.lang.String)
	 */
	@Override
	public String write(String commandType, String command) throws DeviceAdapterException {

		HashMap <String, String> commandMap = new HashMap<String, String>();
		commandMap.put(commandType, command);
		return (String)this.commandResponseMap.get(commandMap);
	}
}
