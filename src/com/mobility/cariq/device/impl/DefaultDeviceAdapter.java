package com.mobility.cariq.device.impl;

import com.mobility.cariq.device.interfaces.AdapterContext;
import com.mobility.cariq.device.interfaces.Device;
import com.mobility.cariq.device.interfaces.DeviceAdapter;
import com.mobility.cariq.device.interfaces.DeviceAdapterException;
import com.mobility.cariq.device.interfaces.ConnectionStatus;


// TODO: Auto-generated Javadoc
/**
 * The Class DefaultDeviceAdapter.
 */
public class DefaultDeviceAdapter implements DeviceAdapter{

	Device device;
	AdapterContext adapterContext;
	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.DeviceAdapter#connect(java.lang.String, com.mobility.cariq.device.interfaces.AdapterContext)
	 */
	@Override
	
	/**
	 * 
	 */
	public Device connect(String deviceUrl, AdapterContext ctx)
			throws DeviceAdapterException {
		try{
			//check if the adapter has context
			if(this.adapterContext != null){
				throw new DeviceAdapterException("Exception: Adapter already has a runtime context");
			}
			else{
				this.adapterContext = ctx;
				if(this.device != null){
					//if Another device is connected
					if(this.device.getStatus().equals(ConnectionStatus.CONNECTED)){
						throw new DeviceAdapterException("Exception: A device is already connected");
					}
					//if connected device in unknown state
					else if(this.device.getStatus().equals(ConnectionStatus.UNKNOWN)){
						throw new DeviceAdapterException("Exception: Device is in UNKNOWN state");
					}
					//if a device is not connected
					else{
						try{
							this.device.disconnect();
							this.device = new DefaultDevice(deviceUrl);
							this.device.connect();
							return this.device;	
						}catch(Exception e){
							throw new DeviceAdapterException("EXCEPTION: The Device connection failed");
						}	
					}	
				}
				else{
					try{
						this.device = new DefaultDevice(deviceUrl);
						this.device.connect();
						return this.device;	
					}catch(Exception e){
						throw new DeviceAdapterException("EXCEPTION: The Device connection failed");
					}	
				}
			}
		}catch(Exception e){
			throw new DeviceAdapterException("EXCEPTION: at com.mobility.cariq.device.impl.DefaultDeviceAdapter"+ e);
		}
	}

	/* (non-Javadoc)
	 * @see com.mobility.cariq.device.interfaces.DeviceAdapter#locate(java.lang.String)
	 */
	@Override
	public Device locate(String deviceId) throws DeviceAdapterException{
		
		try{
			if(this.adapterContext != null){
				if(this.device.getStatus().equals(ConnectionStatus.CONNECTED)){
					if(this.device.getId().equals(deviceId)){
						return this.device;
					}
					else{
						throw new DeviceAdapterException("Exception: Device is already connected to Adapter");
					}
				}
				else if(this.device.getStatus().equals(ConnectionStatus.UNKNOWN)){
					throw new DeviceAdapterException("Exception: The Adapter is connected to UNKNOWN Device");
				}
				else{
					try{
						this.device = new DefaultDevice(deviceId);
						this.device.connect();
						return this.device;
					}catch(Exception e){
						throw new DeviceAdapterException("Exception: No device with DeviceID"+this.device.getId()+" could be located", e);
					}
				}
			}
			else{
				throw new DeviceAdapterException("Exception: Current adapter has no context!");
			}
		}catch(Exception e){
			throw new DeviceAdapterException("Exception at com.mobility.cariq.device.impl.DefaultDeviceAdapter"+ e);
		}
	}

	
}
