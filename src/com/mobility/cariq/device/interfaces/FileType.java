/*
 * 
 */
package com.mobility.cariq.device.interfaces;

// TODO: Auto-generated Javadoc
/**
 * The Enum FileType.
 * Defines the Types of files supported by a Device
 * Can be redefined to support correct types,
 * Current values are placeholder
 * @author Joy Bhattacherjee
 */
public enum FileType {
	
	/** The hex. */
	HEX,
	/** The stream. */
	STREAM;
}
