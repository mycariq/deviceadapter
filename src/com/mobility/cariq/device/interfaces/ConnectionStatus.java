package com.mobility.cariq.device.interfaces;

public enum ConnectionStatus {
	CONNECTED, DISCONNECTED, UNKNOWN;
}
