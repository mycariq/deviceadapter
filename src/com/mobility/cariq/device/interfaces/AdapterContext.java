/*
 * 
 */
package com.mobility.cariq.device.interfaces;

import java.io.Serializable;
import java.util.HashMap;
import java.lang.String;

// TODO: Auto-generated Javadoc
/**
 * The Class AdapterContext.
 * 
 * This class should specifies the run-time context / properties
 * for a DeviceAdapter in the form of key-value pairs of
 * <property, value> 
 */
public class AdapterContext implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3015895242383135400L;
	
	/** The adapter context map.
	 *  holding
	 *  <property, value> pairs
	 *  */
	HashMap<String, Object> adapterContextMap;
	
	/**
	 * Gets the value of a specific property key
	 *
	 * @param key the key
	 * @return the value
	 */
	Object getValue(String key){
		
		return this.adapterContextMap.get(key);
	}
	
	/**
	 * Sets the value of a property
	 *
	 * @param key the key
	 * @param value the value
	 */
	void setValue(String key, Object value){
		this.adapterContextMap.put(key, value);
	}
}
