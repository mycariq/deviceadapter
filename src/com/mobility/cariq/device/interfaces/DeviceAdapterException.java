/*
 * 
 */
package com.mobility.cariq.device.interfaces;

import java.lang.String;

// TODO: Auto-generated Javadoc
/**
 * The Class DeviceAdapterException.
 * Defines the Exception class for Cariq Mobility Device Interfaces
 * All exceptions thrown from the package should be wrapped as a
 * DeviceAdapterException
 * @author Joy Bhattacherjee
 */
public class DeviceAdapterException extends Exception{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7811396685316932656L;

	/**
	 * Instantiates a new device adapter exception.
	 */
	public DeviceAdapterException(){
		super();
	}
	
	/**
	 * Instantiates a new device adapter exception.
	 * with a String message
	 * @param message the message
	 */
	public DeviceAdapterException(String message){
		super(message);
	}
	
	/**
	 * Instantiates a new device adapter exception.
	 * with a String message and a Throwable cause
	 * @param message the message
	 * @param cause the cause
	 */
	public DeviceAdapterException(String message, Throwable cause){
		super(message, cause);
	}
	
	/**
	 * Instantiates a new device adapter exception.
	 * with a Throwable cause 
	 * @param cause the cause
	 */
	public DeviceAdapterException(Throwable cause){
		super(cause);
	}	
}