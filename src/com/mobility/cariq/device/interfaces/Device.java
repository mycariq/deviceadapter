/*
 * 
 */
package com.mobility.cariq.device.interfaces;
import java.io.File;
// TODO: Auto-generated Javadoc

/**
 * The Interface Device.
 * <p>
  The Device Interface defines a prototype device which should be ideally implemented as
  any generic mobility device that have specific communication protocols.
   viz. Bluetooth, Bluetooth Smart, Bluetooth Low Energy, Wi-Fi
 *
 * @author Joy Bhattacherjee
 */
public interface Device {
	
	/**
	 * Gets the id of the Device
	 * a DeviceId is a URI for that device
	 *
	 * @return the id of the Device
	 */
	String getId();
	
	/**
	 * Connects to the Device, or
	 * throw an Exception
	 *
	 * @throws DeviceAdapterException the device adapter exception
	 */
	void connect() throws DeviceAdapterException;
	
	/**
	 * Gets the status of the Device, or
	 * throws an Exception 
	 * should return a value defined in the
	 * ConnectionStatus enumeration
	 * The returned status code should be used for Exception Handling
	 *
	 * @return the status of the Device
	 * @throws DeviceAdapterException the device adapter exception
	 */
	ConnectionStatus getStatus() throws DeviceAdapterException;
	
	/**
	 * Disconnects the Device, or
	 * throws an Exception
	 *
	 * @throws DeviceAdapterException the device adapter exception
	 */
	void disconnect() throws DeviceAdapterException;
	 
	/**
	 * Reads a File from the Device, or
	 * throws an Exception.
	 * Each File is defined by a fileId and a fileType.
	 * fileTypes are as defined in the FileType enumeration.
	 * @param fileId the file id
	 * @param fileType the file type
	 * @return the file
	 * @throws DeviceAdapterException the device adapter exception
	 */
	File readFile(String fileId, FileType fileType) throws DeviceAdapterException; // read file from the device of given type and id
	
	/**
	 * Writes a file to the Device, or
	 * throws an Exception.
	 * Each File is defined by a fileId and a fileType.
	 * fileTypes are as defined in the FileType enumeration.
	 * @param file the file
	 * @param fileType the file type
	 * @return String fileId
	 * @throws DeviceAdapterException the device adapter exception
	 */
	String writeFile(File file, FileType fileType) throws DeviceAdapterException;
	
	/**
	 * Reads a String from the Device, or
	 * throws an Exception.
	 * 
	 * @return Arbitrary String
	 * @throws DeviceAdapterException the device adapter exception
	 */
	String read() throws DeviceAdapterException; // simple read what device wants to say
	
	/**
	 * Reads a String from the Device, or
	 * throws an Exception.
	 * The String parameter is generally a wrapper around some Stream Data.
	 * 
	 * @param stream the stream
	 * @return the string Stream Data
	 * @throws DeviceAdapterException the device adapter exception
	 */
	String read(String stream) throws DeviceAdapterException; // read from device on a channel/type of data
	
	/**
	 * Writes a String command to a Device, or
	 * throws an Exception.
	 * The String Response to a commandType, command pair
	 * is to be handled by specific implementations of the Device Interface
	 * 
	 * @param commandType the command type
	 * @param command the command
	 * @return the string command Response
	 * @throws DeviceAdapterException the device adapter exception
	 */
	String write(String commandType, String command) throws DeviceAdapterException; // write to device - an instruction - return corresponding response - blocking call
}
