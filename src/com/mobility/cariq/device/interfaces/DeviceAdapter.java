/*
 * 
 */
package com.mobility.cariq.device.interfaces;

// TODO: Auto-generated Javadoc
/**
 * The Interface DeviceAdapter.
 *  The DeviceAdapter Interface defines a prototype DeviceAdapter which should be implemented as
 * 	a specific DeviceAdpater on the Mobile Platform side (Android, iOS) that should be able to connect
 * 	to any specific Device implementation, over a predefined protocol.
 * @author Joy Bhattacherjee
 */
public interface DeviceAdapter{

	/**
	 * connect to a device - provide a context - name value pair for additional info.
	 *
	 * @param deviceUrl the device url
	 * @param ctx the AdapterContext object that the DeviceAdapter should run with
	 * @return the device object
	 * @throws DeviceAdapterException the device adapter exception
	 */
	Device connect(String deviceUrl, AdapterContext ctx) throws DeviceAdapterException;
	
	/**
	 * locate an existing device from Id.
	 *
	 * @param deviceId the device id
	 * @return the device
	 * @throws DeviceAdapterException the device adapter exception
	 */
	Device locate(String deviceId) throws DeviceAdapterException;
}
